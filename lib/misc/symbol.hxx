/**
 ** \file misc/symbol.hxx
 ** \brief Inline implementation of misc::symbol.
 */

#pragma once

#include <misc/symbol.hh>

namespace misc
{

  inline symbol&
  symbol::operator=(const symbol& rhs)
  {
    this->obj_= rhs.obj_;
    return *this;
  // FIXedME: Some code was deleted here.
  }

  inline bool
  symbol::operator==(const symbol& rhs) const
  {
    return this->obj_ == rhs.obj_;
  // FIXedME: Some code was deleted here.
  }

  inline bool
  symbol::operator!=(const symbol& rhs) const
  {
    return this->obj_ != rhs.obj_;
  // FIXedME: Some code was deleted here.
  }

  inline std::ostream&
  operator<<(std::ostream& ostr, const symbol& the)
  {
    return ostr << the.get();
  // FIXedME: Some code was deleted here.
  }

}
