/**
 ** \file ast/if-exp.cc
 ** \brief Implementation of ast::IfExp.
 */

#include <ast/visitor.hh>
#include <ast/if-exp.hh>

namespace ast
{
  IfExp::IfExp(const Location& location, Exp* if_exp, Exp* then_exp, Exp* else_exp)
    : Exp(location)
    , if_(if_exp)
    , then_(then_exp)
    , else_(else_exp)
  {}
  void IfExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  void IfExp::accept(Visitor& v)
  {
    v(*this);
  }

  IfExp::~IfExp()
  {
    delete if_;
    delete then_;
    delete else_;
  }
  // FIXME: Some code was deleted here.

} // namespace ast

