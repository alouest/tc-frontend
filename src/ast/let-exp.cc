/**
 ** \file ast/let-exp.cc
 ** \brief Implementation of ast::LetExp.
 */

#include <ast/visitor.hh>
#include <ast/let-exp.hh>

namespace ast
{
  LetExp::LetExp(const Location& location, DecsList* decs,
		 Exp* exps)
  : Exp(location)
  , decs_(decs)
  , exps_(exps)
  {}
  void LetExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  void LetExp::accept(Visitor& v)
  {
    v(*this);
  }

  LetExp::~LetExp()
  {
    delete decs_;
    delete exps_;
  }
  // FIXedME: Some code was deleted here.

} // namespace ast

