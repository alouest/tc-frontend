/**
 ** \file ast/assign-exp.hxx
 ** \brief Inline methods of ast::AssignExp.
 */

#pragma once

#include <ast/assign-exp.hh>

namespace ast
{
  inline
  const Var& AssignExp::var_get() const
  {
    return *var_;
  }
  
  inline
  Var& AssignExp::var_get()
  {
    return *var_;
  }

  inline
  const Exp& AssignExp::val_get() const
  {
    return *val_;
  }

  inline
  Exp& AssignExp::val_get()
  {
    return *val_;
  }
  // FIXMEed: Some code was deleted here.

} // namespace ast

