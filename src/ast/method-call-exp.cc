/**
 ** \file ast/method-call-exp.cc
 ** \brief Implementation of ast::MethodCallExp.
 */

#include <ast/visitor.hh>
#include <ast/method-call-exp.hh>

namespace ast
{
  MethodCallExp::MethodCallExp(const Location& loc,
    const misc::symbol& name,
    const std::vector<Exp*>* arg,
    Var* obj)
    : CallExp(loc, name, arg)
    , obj_(obj)
  {}

  MethodCallExp::~MethodCallExp()
  {
    if (arg_ != nullptr)
    {
      for (auto i : *arg_)
        delete i;
      delete (arg_);
    }
    delete (obj_);
  }

  void MethodCallExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void MethodCallExp::accept(Visitor& v)
  {
    v(*this);
  }
  // FIXME: Some code was deleted here.

} // namespace ast

