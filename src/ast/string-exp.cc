/**
 ** \file ast/string-exp.cc
 ** \brief Implementation of ast::StringExp.
 */

#include <ast/visitor.hh>
#include <ast/string-exp.hh>

namespace ast
{
  StringExp::StringExp(const Location& location, const std::string& string)
  : Exp(location)
  {
    string_ = new std::string(string);
  }

  StringExp::~StringExp()
  {
    delete string_;
  }
  
  void StringExp::accept(Visitor& v)
  {
    v(*this);
  }
  void StringExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  // FIXedME: Some code was deleted here.

} // namespace ast

