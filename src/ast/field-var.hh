/**
 ** \file ast/field-var.hh
 ** \brief Declaration of ast::FieldVar.
 */

#pragma once

#include <ast/var.hh>
#include <misc/symbol.hh>

namespace ast
{

  /// FieldVar.
  class FieldVar : public Var
  {
  public:
    FieldVar(const Location& loc, Var* var, const misc::symbol& name);
    FieldVar(const AssignExp&) = delete;
    FieldVar& operator=(const FieldVar&) = delete;

    virtual ~FieldVar();

    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    const Var& var_get() const;
    Var& var_get();
    const misc::symbol& name_get() const;
    misc::symbol& name_get();
  protected:
    Var* var_;
    misc::symbol name_;
  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/field-var.hxx>

