/**
 ** \file ast/object-exp.cc
 ** \brief Implementation of ast::ObjectExp.
 */

#include <ast/visitor.hh>
#include <ast/object-exp.hh>

namespace ast
{

  ObjectExp::ObjectExp(const Location& location, NameTy* type_id)
  : Exp(location)
  , id_(type_id)
  {}

  void ObjectExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void ObjectExp::accept(Visitor& v)
  {
    v(*this);
  }

  ObjectExp::~ObjectExp()
  {
    delete id_;
  }
  // FIXedME: Some code was deleted here.

} // namespace ast

