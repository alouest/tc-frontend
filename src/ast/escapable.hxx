/**
 ** \file ast/escapable.hxx
 ** \brief Inline methods of ast::Escapable.
 */

#pragma once

#include <ast/escapable.hh>

namespace ast
{
  inline
  int Escapable::depth_get()
  {
    return depth_;
  }

  inline
  const int Escapable::depth_get() const
  {
    return depth_;
  }
  
  inline
  void Escapable::depth_set(int depth)
  {
    depth_ = depth;
  }

  inline
  void Escapable::escaped_set(bool escaped)
  {
    escaped_ = escaped;
  }

  inline
  bool Escapable::escaped_get()
  {
    return escaped_;
  }

  inline
  const bool Escapable::escaped_get() const
  {
    return escaped_;
  }

  inline
  bool Escapable::used_get()
  {
    return used_;
  }

  inline
  void Escapable::used_set()
  {
    used_ = true;
  }

  // FIXME: Some code was deleted here.

} // namespace ast

