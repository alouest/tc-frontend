/**
 ** \file ast/let-exp.hh
 ** \brief Declaration of ast::LetExp.
 */

#pragma once

#include <ast/decs-list.hh>
#include <ast/exp.hh>
#include <misc/contract.hh>

namespace ast
{

  /// LetExp.
  class LetExp : public Exp
  {
  public:
    LetExp(const Location& location, DecsList* decs,
	   Exp* exps);
    LetExp(const LetExp&) = delete;
    LetExp& operator=(const LetExp&) = delete;

    virtual ~LetExp();

    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    const DecsList& get_decs() const;
    DecsList& get_decs();
    const Exp& get_exps() const;
    Exp& get_exps();

  protected:
    DecsList* decs_;
    Exp* exps_;
  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/let-exp.hxx>

