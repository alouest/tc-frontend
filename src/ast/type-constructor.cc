/**
 ** \file ast/type-constructor.cc
 ** \brief Implementation of ast::TypeConstructor.
 */

#include <ast/visitor.hh>
#include <ast/type-constructor.hh>

namespace ast
{
  TypeConstructor::TypeConstructor()
  {}
  
  TypeConstructor::~TypeConstructor()
  {
    if (created_type_ != nullptr)
      delete created_type_;
  }
  


  // FIXME: Some code was deleted here.

} // namespace ast

