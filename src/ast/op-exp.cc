/**
 ** \file ast/op-exp.cc
 ** \brief Implementation of ast::OpExp.
 */

#include <ast/visitor.hh>
#include <ast/op-exp.hh>

namespace ast
{

  OpExp::OpExp(const Location& location, Exp* left, OpExp::Oper oper,
               Exp* right)
    : Exp(location)
    , left_(left)
    , oper_(oper)
    , right_(right)
  {}

  OpExp::~OpExp()
  {
    delete left_;
    delete right_;
  }

  void
  OpExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  OpExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

std::string str(ast::OpExp::Oper oper)
{
  if (oper == ast::OpExp::Oper::add)
    return " + ";
  if (oper == ast::OpExp::Oper::sub)
    return " - ";
  if (oper == ast::OpExp::Oper::mul)
    return " * ";
  if (oper == ast::OpExp::Oper::div)
    return " / ";
  if (oper == ast::OpExp::Oper::eq)
    return " = ";
  if (oper == ast::OpExp::Oper::ne)
    return " <> ";
  if (oper == ast::OpExp::Oper::lt)
    return " < ";
  if (oper == ast::OpExp::Oper::gt)
    return " > ";
  if (oper == ast::OpExp::Oper::le)
    return " <= ";
  if (oper == ast::OpExp::Oper::ge)
    return " >= ";
}
  // FIXedME: Some code was deleted here.

