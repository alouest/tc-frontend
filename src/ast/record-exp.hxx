/**
 ** \file ast/record-exp.hxx
 ** \brief Inline methods of ast::RecordExp.
 */

#pragma once

#include <ast/record-exp.hh>

namespace ast
{
  inline const std::vector<FieldInit*>& RecordExp::get_fields() const
  {
    return *fields_;
  }
  inline std::vector<FieldInit*>& RecordExp::get_fields()
  {
    return *fields_;
  }
  inline const NameTy& RecordExp::get_type() const
  {
    return *type_;
  }
  inline NameTy& RecordExp::get_type()
  {
    return *type_;
  }

  // FIXedME: Some code was deleted here.

} // namespace ast

