/**
 ** \file ast/object-exp.hh
 ** \brief Declaration of ast::ObjectExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/name-ty.hh>

namespace ast
{

  /// ObjectExp.
  class ObjectExp : public Exp
  {
    public:
    ObjectExp(const Location&, NameTy* type_id);
    virtual ~ObjectExp();
    ObjectExp(const ObjectExp&) = delete;
    ForExp& operator=(const ForExp&) = delete;
    const NameTy& get_id() const;
    NameTy& get_id();
    virtual void accept(ConstVisitor& v) const override;
    virtual void accept(Visitor& v) override;
    private:
    NameTy* id_;
  // FIXME: Some code was deleted here.
  };

} // namespace ast

#include <ast/object-exp.hxx>

