/**
 ** \file ast/if-exp.hh
 ** \brief Declaration of ast::IfExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/seq-exp.hh>

namespace ast
{

  /// IfExp.
  class IfExp : public Exp
  {
  public:
    IfExp(const Location& location, Exp* if_exp, Exp* then_exp, Exp* else_exp);
    IfExp(const IfExp&) = delete;
    IfExp& operator=(const ForExp&) = delete;
    virtual ~IfExp();
    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;
    const Exp& get_if() const;
    Exp& get_if();
    const Exp& get_then() const;
    Exp& get_then();
    const Exp* get_else() const;
    Exp* get_else();
  protected:
    Exp* if_;
    Exp* then_;
    Exp* else_;
  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/if-exp.hxx>

