/**
 ** \file ast/record-exp.hh
 ** \brief Declaration of ast::RecordExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/field-init.hh>
#include <ast/name-ty.hh>

namespace ast
{

  /// RecordExp.
  class RecordExp : public Exp
  {
    public:
    RecordExp(const Location& location, NameTy* type, std::vector<FieldInit*>* fields);
    virtual ~RecordExp();
    RecordExp(const RecordExp&) = delete;
    RecordExp& operator=(const RecordExp&) = delete;
    const std::vector<FieldInit*>& get_fields() const;
    std::vector<FieldInit*>& get_fields();
    const NameTy& get_type() const;
    NameTy& get_type();
    void accept(ConstVisitor& v) const;
    void accept(Visitor& v);
    protected:
    std::vector<FieldInit*>* fields_;
    NameTy* type_;
  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/record-exp.hxx>

