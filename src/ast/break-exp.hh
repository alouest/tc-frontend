/**
 ** \file ast/break-exp.hh
 ** \brief Declaration of ast::BreakExp.
 */

#pragma once

#include <ast/exp.hh>

namespace ast
{

  /// BreakExp.
  class BreakExp : public Exp
  {
  public:
    BreakExp(const Location& loc);
    BreakExp(const BreakExp&) = delete;
    BreakExp& operator=(const BreakExp&) = delete;

    virtual ~BreakExp();

    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    Exp* def_get();
    const Exp* def_get() const;
    void def_set(Exp* def);

  protected:
    Exp* def_;

  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/break-exp.hxx>

