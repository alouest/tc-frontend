/**
 ** \file ast/if-exp.hxx
 ** \brief Inline methods of ast::IfExp.
 */

#pragma once

#include <ast/if-exp.hh>

namespace ast
{
  
  inline Exp& IfExp::get_if()
  {
    return *if_;
  }
  inline const Exp& IfExp::get_if() const
  {
    return *if_;
  }
  inline Exp& IfExp::get_then()
  {
    return *then_;
  }
  inline const Exp& IfExp::get_then() const
  {
    return *then_;
  }
  inline Exp* IfExp::get_else()
  {
    return else_;
  }
  inline const Exp* IfExp::get_else() const
  {
    return else_;
  }
  // FIXedME: Some code was deleted here.

} // namespace ast

