/**
 ** \file ast/method-call-exp.hh
 ** \brief Declaration of ast::MethodCallExp.
 */

#pragma once

#include <ast/call-exp.hh>
#include <ast/method-dec.hh>
#include <ast/var.hh>

namespace ast
{

  /** \class ast::MethodCallExp
   ** \brief Method call.
   **
   ** A method call is \em not a function call in the strict sense
   ** of object-oriented programming.  Inheritance is used as a
   ** factoring tool here.
   */

  class MethodCallExp : public CallExp
  {
  public:
    MethodCallExp(const Location& loc, const misc::symbol& name,
    const std::vector<Exp*>* arg, Var* obj);
    MethodCallExp(const CallExp&) = delete;
    MethodCallExp& operator=(const CallExp&) = delete;

    virtual ~MethodCallExp();

    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    Var& obj_get();
    const Var& obj_get() const;
  protected:
    Var* obj_;
  // FIXME: Some code was deleted here.
  };

} // namespace ast

#include <ast/method-call-exp.hxx>

