/**
 ** \file ast/array-exp.hh
 ** \brief Declaration of ast::ArrayExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/name-ty.hh>

namespace ast
{

  /// ArrayExp.
  class ArrayExp : public Exp
  {
  public:
    ArrayExp(const Location& loc, Exp* row, Exp* in, NameTy* name);
    ArrayExp(const ArrayExp&) = delete;
    ArrayExp& operator=(const ArrayExp&) = delete;

    virtual ~ArrayExp();

    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    const Exp& row_get() const;
    Exp& row_get();
    const Exp& of_get() const;
    Exp& of_get();
    const NameTy& name_get() const;
    NameTy& name_get();
  protected:
    NameTy* name_;
    Exp* of_;
    Exp* row_;
  // FIXMEed: Some code was deleted here.
  };

} // namespace ast

#include <ast/array-exp.hxx>

