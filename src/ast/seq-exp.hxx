/**
 ** \file ast/seq-exp.hxx
 ** \brief Inline methods of ast::SeqExp.
 */

#pragma once

#include <ast/seq-exp.hh>

namespace ast
{
  inline std::vector<Exp*>& SeqExp::get_exps()
  {
    return *exps_;
  }
  inline const std::vector<Exp*>& SeqExp::get_exps() const
  {
    return *exps_;
  }

  // FIXME: Some code was deleted here.

} // namespace ast

