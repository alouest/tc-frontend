/**
 ** \file ast/seq-exp.cc
 ** \brief Implementation of ast::SeqExp.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/seq-exp.hh>

namespace ast
{
  SeqExp::SeqExp(const Location& location, std::vector<Exp*>* exps)
  : Exp(location)
  , exps_(exps)
  {}
  SeqExp::~SeqExp()
  {
    for (auto i : *exps_)
      delete(i);
    delete exps_;
  }
  void SeqExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  void SeqExp::accept(Visitor& v)
  {
    v(*this);
  }

  // FIXedME: Some code was deleted here.

} // namespace ast

