/**
 ** \file ast/record-ty.cc
 ** \brief Implementation of ast::RecordTy.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/record-ty.hh>

namespace ast
{
    RecordTy::RecordTy(const Location& location, std::vector<Field*>* tyfields)
    : Ty(location)
    , tyfields_(tyfields)
    {}

    RecordTy::~RecordTy()
    {
      if (tyfields_ != nullptr)
	for (auto i : *tyfields_)
	  delete i;
	delete tyfields_;
    }

    void RecordTy::accept(Visitor& v)
    {
      v(*this);
    }
  
    void RecordTy::accept(ConstVisitor& v) const
    {
      v(*this);
    }

  // FIXedME: Some code was deleted here.

} // namespace ast

