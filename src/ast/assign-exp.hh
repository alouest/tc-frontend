/**
 ** \file ast/assign-exp.hh
 ** \brief Declaration of ast::AssignExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/var.hh>

namespace ast
{

  /// AssignExp.
  class AssignExp : public Exp
  {
  public:
    AssignExp(const Location& loc, Var* var, Exp* val);
    AssignExp(const AssignExp&) = delete;
    AssignExp& operator=(const AssignExp&) = delete;

    virtual ~AssignExp();

    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    const Var& var_get() const;
    Var& var_get();
    const Exp& val_get() const;
    Exp& val_get();
  protected:
    Var* var_;
    Exp* val_;
  // FIXedME: Some code was deleted here.
  };

} // namespace ast

#include <ast/assign-exp.hxx>

