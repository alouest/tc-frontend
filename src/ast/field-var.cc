/**
 ** \file ast/field-var.cc
 ** \brief Implementation of ast::FieldVar.
 */

#include <ast/visitor.hh>
#include <ast/field-var.hh>

namespace ast
{
  FieldVar::FieldVar(const Location& loc, Var* var, const misc::symbol& name)
    : Var(loc)
    , var_(var)
    , name_(name)
  {}

  FieldVar::~FieldVar()
  {
    delete var_;
  }
  
  void FieldVar::accept(Visitor& v)
  {
    v(*this);
  }

  void FieldVar::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  // FIXedME: Some code was deleted here.

} // namespace ast

