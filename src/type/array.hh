/**
 ** \file type/array.hh
 ** \brief The class Array.
 */
#pragma once

#include <type/fwd.hh>
#include <type/type.hh>

namespace type
{

  /// Array types.
  class Array : public Type
  {
  public:
    Array(const misc::symbol& name, const Type& elt);
    
    void accept(ConstVisitor& v) const override;
    void accept(Visitor& v) override;

    
    //    misc::symbol& name_get();
    const misc::symbol& name_get() const;
    //Type& elt_get();
    const Type& elt_get() const;
    
    bool compatible_with(const Type& other) const override;

    // FIXME: Some code was deleted here.
  protected:
    const misc::symbol& name_;
    const Type& elt_;
  };

} // namespace type

#include <type/array.hxx>
