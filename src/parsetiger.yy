%require "3.0.2"
%skeleton "lalr1.cc"
%expect 0
%defines

%define api.token.prefix {TOK_}
%define api.token.constructor
%define api.value.type variant

%code
{
  #include <iostream>
  yy::parser::symbol_type yylex();
}

%token END_OF_FILE 0"<EOF>"
%token DIGIT
%token ID
%token ARRAY "array"
%token IF "if"
%token THEN "then"
%token WHILE "while"
%token ELSE "else"
%token FOR "for"
%token TO "to"
%token DO "do"
%token LET "let"
%token IN "in"
%token END "end"
%token OF "of"
%token BREAK "break"
%token NIL "nil"
%token FUNCTION "function"
%token VAR "var"
%token TYPE "type"
%token IMPORT "import"
%token PRIMITIVE "primitive"
%token CLASS "class"
%token EXTENDS "extends"
%token METHOD "method"
%token NEW "new"
%token COMMA ","
%token ASSIGN ":="
%token COLON ":"
%token SEMICOLON ";"
%token LPARENTHESIS "("
%token RPARENTHESIS ")"
%token LBRACKET "["
%token RBRACKET "]"
%token LBRACE "{"
%token RBRACE "}"
%token DOT "."
%token STRING
%token PLUS "+"
%token MINUS "-"
%token STAR "*"
%token SLASH "/"
%token EQUAL "="
%token DIFFERENT "<>"
%token LOWEREQUAL "<="
%token GREATEREQUAL ">="
%token LOWER "<"
%token GREATER ">"
%token AND "&"
%token OR "|"

%right ELSE THEN
%nonassoc TO DO OF ASSIGN
%left OR
%left AND
%nonassoc DIFFERENT GREATER LOWER LOWEREQUAL GREATEREQUAL EQUAL
%left PLUS MINUS
%left STAR SLASH

%start program

%%

program :
        exp
        | decs
        ;

recordfields :
         %empty
         | ID "=" exp recordfields2
         ;

recordfields2 :
          %empty
          | "," ID "=" exp recordfields2
          ;

argfields :
          %empty
          | exp argfields2
          ;

argfields2 :
           %empty
           | "," exp argfields2
           ;

exp :
    "nil"
    | DIGIT
    | STRING
    | ID "[" exp "]" "of" exp
    | ID "{" recordfields "}"
    | "new" ID
    | lvalue
    | ID "(" argfields ")"
    | lvalue "." ID "(" argfields ")"
    | "-" exp
    | exp "+" exp
    | exp "-" exp
    | exp "*" exp
    | exp "/" exp
    | exp "=" exp
    | exp "<>" exp
    | exp "<" exp
    | exp ">" exp
    | exp "<=" exp
    | exp ">=" exp
    | exp "&" exp
    | exp "|" exp
    | "(" exps ")"
    | lvalue ":=" exp
    | "if" exp "then" exp "else" exp
    | "if" exp "then" exp
    | "while" exp "do" exp
    | "for" ID ":=" exp "to" exp "do" exp
    | "break"
    | "let" decs "in" exps "end"
    ;

lvalue :
       ID
       | lvalue2
       ;

lvalue2 :
        ID "[" exp "]"
        | lvalue "." ID
        | lvalue2 "[" exp "]"
        ;

exps :
      %empty
     | exp exps2
     ;

exps2 :
      %empty
      | ";" exp exps2
      ;

decs :
     %empty
     | dec decs
     ;

dec :
    "type" ID "=" ty
    | "class" ID "{" classfields "}"
    | "class" ID "extends" ID "{" classfields "}"
    | vardec
    | "function" ID "(" tyfields ")" "=" exp
    | "function" ID "(" tyfields ")" ":" ID "=" exp
    | "primitive" ID "(" tyfields ")"
    | "primitive" ID "(" tyfields ")" ":" ID
    | "import" STRING
    ;

vardec :
       "var" ID ":" ID ":=" exp
       | "var" ID ":=" exp
       ;

classfields :
            %empty
            | classfield classfields
            ;

classfield :
           vardec
           | "method" ID "(" tyfields ")" "=" exp
           | "method" ID "(" tyfields ")" ":" ID "=" exp
           ;

ty :
   ID
   | "{" tyfields "}"
   | "array" "of" ID
   | "class" "{" classfields "}"
   | "class" "extends" ID "{" classfields "}"
   ;

tyfields :
         %empty
         | ID ":" ID tyfields2
         ;

tyfields2 :
          %empty
          | "," ID ":" ID tyfields2
          ;

%%



void yy::parser::error(const std::string& msg)
{
  std::cerr << msg << std::endl;
  exit(3);
}

extern FILE * yyin;

int main(int argc, char* argv[])
{
  if (argc > 1)
    yyin = fopen(argv[1], "r");
  else
    yyin = stdin;
  yy::parser parser;
  parser.parse();
}
