/**
 ** \file bind/tasks.hh
 ** \brief Bind module related tasks.
 */
#pragma once

#include <ast/decs-list.hh>
#include <task/libtask.hh>
#include <bind/renamer.hh>
#include <iostream>
#include <bind/binder.hh>
#include <misc/contract.hh>
#include <ast/libast.hh>



namespace bind
{
  namespace tasks
  { 
    TASK_GROUP("3. Binder");

    TASK_DECLARE("bound", "bind the variable", bind, "parse");
    TASK_DECLARE("b|bindings-compute", "compute the bindings"
		 , bindings_compute, "parse");
    TASK_DECLARE("B|bindings-display", "display the bindings",
		 binding_display, "");
    TASK_DECLARE("rename", "rename the variable", rename, "bindings-compute");
  }
}
  // FIXME: Some code was deleted here.
