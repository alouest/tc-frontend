/**
 ** \file bind/renamer.hxx
 ** \brief Template methods of bind::Renamer.
 */

#pragma once

#include <bind/renamer.hh>
#include <misc/algorithm.hh>
namespace bind
{

  // FIXedME: Some code was deleted here.

  template <class E, class Def>
  void
  Renamer::visit(E& e, const Def* def)
  {
    if (def != nullptr)
    {
      super_type::operator()(e);
      e.name_set(new_name(*def));
    }
  // FIXedME: Some code was deleted here.
  }

  template <typename Def>
  misc::symbol Renamer::new_name(const Def& e)
  {
    auto found = new_names_.find(&e);
    if (found != new_names_.end())
      return found->second;
    return new_name_compute(e);
  }
  
  template <>
  inline misc::symbol Renamer::new_name_compute(const ast::FunctionDec& e)
  {
    if (e.body_get() == nullptr || e.name_get() == "_main")
      return e.name_get();
    auto i = misc::symbol::fresh(e.name_get());
    return misc::put(new_names_, &e, i)->second;
  }

  template <typename Def>
  inline misc::symbol Renamer::new_name_compute(const Def& e)
  {
    if (e.name_get() == "_main")
      return e.name_get();
    auto i = misc::symbol::fresh(e.name_get());
    new_names_.insert(std::pair<const ast::Dec*, misc::symbol>(&e, i));
    return i;
  }

} // namespace bind
