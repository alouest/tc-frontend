/**
 ** \file bind/libbind.hh
 ** \brief Interface of the bind module.
 */

#pragma once

#include <ast/all.hh>
#include <misc/error.hh>
#include <bind/binder.hh>
namespace bind
{
  const misc::error& bind(ast::Ast& program);
  extern bool bind_object;
}
// FIXME: Some code was deleted here.
