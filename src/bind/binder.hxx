/**
 ** \file bind/binder.hxx
 ** \brief Inline methods of bind::Binder.
 **/

#pragma once

#include <bind/binder.hh>

namespace bind
{

  /*-----------------.
  | Error handling.  |
  `-----------------*/
  template <class D>
  void Binder::redefinition(const D& k, const D& e)
  {
    error_ << misc::error::error_type::bind << k.location_get() <<
      ": redefinition of: " << k.name_get() << "\n" << e.location_get() <<
      ": First definition here." << std::endl;
  }

  template <typename T>
  void Binder::undeclared(const std::string& k, const T& e)
  {
    error_ << misc::error::error_type::bind << e.location_get()
	   << ": Undeclared variable: "
	   << k << "\n";
  }

  template <typename T>
  void Binder::break_error(const T& e)
  {
    error_ << misc::error::error_type::bind << e
	   << ": Break outside any loop\n";
  }
  // FIXME: Some code was deleted here (Error reporting).

  /*-------------------.
  | Definition sites.  |
  `-------------------*/

  // FIXME: Some code was deleted here.


  /*------------------.
  | Visiting /Decs/.  |
  `------------------*/
  
  template <class D>
  void
  Binder::decs_visit(ast::AnyDecs<D>& e)
  {
    // Shorthand.
    using decs_type = ast::AnyDecs<D>;
    for (auto i : e.decs_get())
      for (auto j : e.decs_get())
	{
	  if ((i->name_get() == j->name_get()) && i != j)
	    redefinition(*i, *j);
	}
    for (auto i : e.decs_get())
      visit_dec_header(*i);
    for (auto i : e.decs_get())
      visit_dec_body(*i);
  // FIXME: Some code was deleted here (Two passes: once on headers, then on bodies).
  }


  
  template <>
  inline void
  Binder::visit_dec_header<ast::FunctionDec>(ast::FunctionDec& e)
  {
    function_.put(e.name_get(), &e);
  }

  template <>
  inline void
  Binder::visit_dec_header<ast::VarDec>(ast::VarDec& e)
  {
    var_.put(e.name_get(), &e);
  }

  template <>
  inline void
  Binder::visit_dec_header<ast::TypeDec>(ast::TypeDec& e)
  {
    type_.put(e.name_get(), &e);
  }

  template <class D>
  inline void
  Binder::visit_dec_body(D& e)
  {
    (*this)(e);
  }
  /* These specializations are in bind/binder.hxx, so that derived
     visitors can use them (otherwise, they wouldn't see them).  */

  // FIXME: Some code was deleted here.

} // namespace bind
