%{ /* -*- C++ -*- */

#include <iostream>
#include <sstream>
#include "parsetiger.hh"

int depth = 0;
extern int error;

#define YY_DECL   \
  yy::parser::symbol_type yylex()

using token = yy::parser::token;


#define TOKEN(name) yy::parser::make_ ## name()

#define yyterminate() return TOKEN(END_OF_FILE)

%}

%option noyywrap

%x COMMENT
%x STRING

/* DEF REGEXP */
BLANK    [ \t]
INT      [0-9]+
ID       [a-zA-Z][a-zA-Z0-9_]*|"_main"
LINE     "\r\n"|"\n"|"\r"|"\n\r"
ESCAPE   "\\a"|"\\b"|"\\\\"|"\\f"|"\\n"|"\\r"|"\\t"|"\\v"|"\\num"|"\\xnum"
BESCAPE  [\].

/* RULES */
%%
<COMMENT>
{
  "\n"      { }
  "/*"      { depth ++; }
  "*/"      { depth --;
              if (depth  <= 0)
	        BEGIN (INITIAL); }
  <<EOF>>   {
              std::cerr << "unfinished comment" << std::endl;
	      exit(2);
            }
  .         { }
}


<STRING>
{
  {ESCAPE}  { }
  "\""      { BEGIN (INITIAL); }
  "\\"      {
              std::cerr << "Unrecognized escape : " << yytext << std::endl;
	      exit(2);
            }
  <<EOF>>   {
              std::cerr << ": unfinished string" << std::endl;
	      exit(2);
            }
  .         { }
}


{BLANK}     { }
{LINE}      { }
{INT}       {
              try
		{
		  auto rand = 0;
		  rand = std::stoi(yytext);
		}
	      catch (std::out_of_range& e)
		{
		  std::cerr << "Integer out of bound : " << yytext << std::endl;
		  exit(2);
		}
              return TOKEN(DIGIT);
            }
"array"     { return TOKEN(ARRAY); }
"if"        { return TOKEN(IF); }
"then"      { return TOKEN(THEN); }
"else"      { return TOKEN(ELSE); }
"while"     { return TOKEN(WHILE); }
"for"       { return TOKEN(FOR); }
"to"        { return TOKEN(TO); }
"do"        { return TOKEN(DO); }
"let"       { return TOKEN(LET); }
"in"        { return TOKEN(IN); }
"end"       { return TOKEN(END);}
"of"        { return TOKEN(OF); }
"break"     { return TOKEN(BREAK); }
"nil"       { return TOKEN(NIL); }
"function"  { return TOKEN(FUNCTION); }
"var"       { return TOKEN(VAR); }
"type"      { return TOKEN(TYPE); }
"import"    { return TOKEN(IMPORT); }
"primitive" { return TOKEN(PRIMITIVE); }
"class"     { return TOKEN(CLASS); }
"extends"   { return TOKEN(EXTENDS); }
"method"    { return TOKEN(METHOD); }
"new"       { return TOKEN(NEW); }
"/*"        {
              depth ++;
 	      BEGIN(COMMENT);
    	    }
","         { return TOKEN(COMMA); }
":"         { return TOKEN(COLON); }
";"         { return TOKEN(SEMICOLON); }
"("         { return TOKEN(LPARENTHESIS); }
")"         { return TOKEN(RPARENTHESIS); }
"["         { return TOKEN(LBRACKET); }
"]"         { return TOKEN(RBRACKET); }
"{"         { return TOKEN(LBRACE); }
"}"         { return TOKEN(RBRACE); }
"."         { return TOKEN(DOT); }
"+"         { return TOKEN(PLUS); }
"-"         { return TOKEN(MINUS); }
"*"         { return TOKEN(STAR); }
"/"         { return TOKEN(SLASH); }
"="         { return TOKEN(EQUAL); }
":="        { return TOKEN(ASSIGN); }
"<>"        { return TOKEN(DIFFERENT); }
"<"         { return TOKEN(LOWER); }
">"         { return TOKEN(GREATER); }
"<="        { return TOKEN(LOWEREQUAL); }
">="        { return TOKEN(GREATEREQUAL); }
"&"         { return TOKEN(AND); }
"|"         { return TOKEN(OR); }
"\""        { BEGIN (STRING); return TOKEN(STRING); }
{ID}        { return TOKEN(ID); }
.           {
              std::cerr << "scan error, unexpected " << yytext << std::endl;
	      exit(2);
	    }
%%
