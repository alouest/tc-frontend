import os

import subprocess
import sys

class colors:
    GREEN =     '\033[0;92m'
    RED =       '\033[5;91m'
    WHITE =     '\033[0m'
    YELLOW =    '\033[0;93m'
    MAGENTA =   '\033[0;95m'
    CYAN =      '\033[0;96m'

n = 0
p = 0
tests_files = sys.argv[2]
tc_bin = sys.argv[1]

for file in os.listdir(tests_files):
        n += 1
        print(colors.CYAN + "-> Test " + str(n) + ": " + colors.WHITE + file)
        print(colors.YELLOW)
        path = tc_bin + " " + tests_files + file
        print(path)
        tmp = subprocess.run(path.split())
        if tmp.returncode == 0:
            print(colors.WHITE + file + colors.GREEN + " passed\n")
            p += 1
        else :
            print(colors.WHITE + file + colors.RED + " failed\n")

tests_files = sys.argv[3]

print(colors.MAGENTA + ">> PRETTY PRINT" + ": ")

for file in os.listdir(tests_files):
        n += 1
        print(colors.CYAN + "-> Test " + str(n) + ": " + colors.WHITE + file)
        path = tc_bin + " " + tests_files + file
        print(path)
        tmp = subprocess.run(path.split())
        if tmp.returncode == 0:
            print(colors.WHITE + file + colors.GREEN + " passed\n")
            p += 1
        else :
            print(colors.WHITE + file + colors.RED + " failed\n")

tests_files = sys.argv[4]

for file in os.listdir(tests_files):
        n += 1
        print(colors.CYAN + "-> Test " + str(n) + ": " + colors.WHITE + file)
        print(colors.YELLOW)
        path = tc_bin + " " + tests_files + file
        print(path)
        tmp = subprocess.run(path.split())
        if tmp.returncode == 3:
            print(colors.WHITE + file + colors.GREEN + " passed\n")
            p += 1
        else :
            print(colors.WHITE + file + colors.RED + " failed\n")

tests_files = sys.argv[6]

for file in os.listdir(tests_files):
        n += 1
        print(colors.CYAN + "-> Test " + str(n) + ": " + colors.WHITE + file)
        print(colors.YELLOW)
        path = tc_bin + " " + tests_files + file
        print(path)
        tmp = subprocess.run(path.split())
        if tmp.returncode == 3:
            print(colors.WHITE + file + colors.GREEN + " passed\n")
            p += 1
        else :
            print(colors.WHITE + file + colors.RED + " failed\n")

tests_files = sys.argv[7]

for file in os.listdir(tests_files):
        n += 1
        print(colors.CYAN + "-> Test " + str(n) + ": " + colors.WHITE + file)
        print(colors.YELLOW)
        path = tc_bin + " " + tests_files + file
        print(path)
        tmp = subprocess.run(path.split())
        if tmp.returncode == 0:
            print(colors.WHITE + file + colors.GREEN + " passed\n")
            p += 1
        else :
            print(colors.WHITE + file + colors.RED + " failed\n")

tests_files = sys.argv[5]

for file in os.listdir(tests_files):
        n += 1
        print(colors.CYAN + "-> Test " + str(n) + ": " + colors.WHITE + file)
        print(colors.YELLOW)
        path = tc_bin + " " + tests_files + file
        print(path)
        tmp = subprocess.run(path.split())
        if tmp.returncode == 0:
            print(colors.WHITE + file + colors.GREEN + " passed\n")
            p += 1
        else :
            print(colors.WHITE + file + colors.RED + " failed\n")


print(colors.WHITE + "-> " + str(n) + " tests was done")
print(colors.GREEN + "-> " + str(p) + " tests passed")
print(colors.RED + "-> " + str(n - p) + " tests failed")
print(colors.WHITE)

