CXX = g++
CXXFLAGS = -std=c++17

all: tc

tc: src/parsetiger.o src/scantiger.o
	$(CXX) $^ -o $@

scan.o: parsetiger.hh

%.o: %.cc
	$(CXX) $(CXXFLAGS) $< -c -o $@

%.cc %.hh: %.yy
	bison $(BISONFLAGS) $< -o $@ --defines=$*.hh

%.cc: %.ll
	flex -o$@ $<
.: parsertiger.cc scantiger.cc

clean:
	rm -f $(CLEANFILES)
